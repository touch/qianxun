package com.qianxun.service.impl;

import com.qianxun.entity.Tag;
import com.qianxun.mapper.TagMapper;
import com.qianxun.service.ITagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@Service
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag> implements ITagService {

}
