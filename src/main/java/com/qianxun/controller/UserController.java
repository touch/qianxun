package com.qianxun.controller;


import com.qianxun.entity.User;
import com.qianxun.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@Controller
@RequestMapping("/qianxun/user")
public class UserController extends BaseController{
    @RequestMapping("/")
    public String user(){
        return "admin/user";
    }

    @RequestMapping("/doGetUser")
    @ResponseBody
    public Map<Object,Object> doGetUser(String phone,String username){
        username = username==null?"":username.trim();
        phone = phone==null?"":phone.trim();

        List<User> userList =  userService.lambdaQuery().like(User::getmPhone,phone).like(User::getmUserName,username).list();;
        return getRightResponse(userList.size(),userList,"获取用户信息成功");
    }



    /**
     * 使用mybatis-plus访问数据库
     * @return
     */
    @RequestMapping("/doGetAllUser")
    @ResponseBody                        //加@ResponseBody 注解表示返回的是具体数据，不加则表示返回的是temples文件夹下面的文件
    public Map<String,Object> doGetAllUser(){
       List<User> userList= userService.list();
       Map<String,Object> map=new HashMap<String,Object>();
       map.put("data",userList);
       map.put("code",0);
       map.put("count",userList==null?0:userList.size());
       return map;
    }
}
