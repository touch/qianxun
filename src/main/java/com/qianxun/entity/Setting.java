package com.qianxun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@TableName("t_setting")
@ApiModel(value = "Setting对象", description = "")
public class Setting implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("关于我们")
    private String mAboutUs;

    @ApiModelProperty("报名须知")
    private String mNotice;

    @ApiModelProperty("安全告知")
    private String mSecurity;

    @ApiModelProperty("隐私政策")
    private String mPrivate;

    @ApiModelProperty("微信支付二维码")
    private String mWechatPayQrCode;

    @ApiModelProperty("支付宝支付二维码")
    private String mAlipayQrCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getmAboutUs() {
        return mAboutUs;
    }

    public void setmAboutUs(String mAboutUs) {
        this.mAboutUs = mAboutUs;
    }
    public String getmNotice() {
        return mNotice;
    }

    public void setmNotice(String mNotice) {
        this.mNotice = mNotice;
    }
    public String getmSecurity() {
        return mSecurity;
    }

    public void setmSecurity(String mSecurity) {
        this.mSecurity = mSecurity;
    }
    public String getmPrivate() {
        return mPrivate;
    }

    public void setmPrivate(String mPrivate) {
        this.mPrivate = mPrivate;
    }
    public String getmWechatPayQrCode() {
        return mWechatPayQrCode;
    }

    public void setmWechatPayQrCode(String mWechatPayQrCode) {
        this.mWechatPayQrCode = mWechatPayQrCode;
    }
    public String getmAlipayQrCode() {
        return mAlipayQrCode;
    }

    public void setmAlipayQrCode(String mAlipayQrCode) {
        this.mAlipayQrCode = mAlipayQrCode;
    }

    @Override
    public String toString() {
        return "Setting{" +
            "id=" + id +
            ", mAboutUs=" + mAboutUs +
            ", mNotice=" + mNotice +
            ", mSecurity=" + mSecurity +
            ", mPrivate=" + mPrivate +
            ", mWechatPayQrCode=" + mWechatPayQrCode +
            ", mAlipayQrCode=" + mAlipayQrCode +
        "}";
    }
}
