package com.qianxun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@TableName("t_ad")
@ApiModel(value = "Ad对象", description = "")
public class Ad implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("广告图的图片url")
    private String mImageUrl;

    @ApiModelProperty("广告图排序字段")
    private Integer mOrder;

    @ApiModelProperty("广告图链接url")
    private String mLinkUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }
    public Integer getmOrder() {
        return mOrder;
    }

    public void setmOrder(Integer mOrder) {
        this.mOrder = mOrder;
    }
    public String getmLinkUrl() {
        return mLinkUrl;
    }

    public void setmLinkUrl(String mLinkUrl) {
        this.mLinkUrl = mLinkUrl;
    }

    @Override
    public String toString() {
        return "Ad{" +
            "id=" + id +
            ", mImageUrl=" + mImageUrl +
            ", mOrder=" + mOrder +
            ", mLinkUrl=" + mLinkUrl +
        "}";
    }
}
