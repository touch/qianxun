package com.qianxun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
@TableName("t_order")
@ApiModel(value = "Order对象", description = "")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer userId;

    @ApiModelProperty("关联的活动id")
    private Integer activityId;

    @ApiModelProperty("报名的排期")
    private String mPlan;

    @ApiModelProperty("出行人信息")
    private String mTraveller;

    @ApiModelProperty("紧急联系人姓名")
    private String mContactName;

    @ApiModelProperty("紧急联系人电话")
    private String mContactPhone;

    @ApiModelProperty("0待支付，1未出行，2待评价，3已完成，4已退订，5已取消")
    private Integer mState;

    @ApiModelProperty("支付转账成功截图url")
    private String mPayImageUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }
    public String getmPlan() {
        return mPlan;
    }

    public void setmPlan(String mPlan) {
        this.mPlan = mPlan;
    }
    public String getmTraveller() {
        return mTraveller;
    }

    public void setmTraveller(String mTraveller) {
        this.mTraveller = mTraveller;
    }
    public String getmContactName() {
        return mContactName;
    }

    public void setmContactName(String mContactName) {
        this.mContactName = mContactName;
    }
    public String getmContactPhone() {
        return mContactPhone;
    }

    public void setmContactPhone(String mContactPhone) {
        this.mContactPhone = mContactPhone;
    }
    public Integer getmState() {
        return mState;
    }

    public void setmState(Integer mState) {
        this.mState = mState;
    }
    public String getmPayImageUrl() {
        return mPayImageUrl;
    }

    public void setmPayImageUrl(String mPayImageUrl) {
        this.mPayImageUrl = mPayImageUrl;
    }

    @Override
    public String toString() {
        return "Order{" +
            "id=" + id +
            ", userId=" + userId +
            ", activityId=" + activityId +
            ", mPlan=" + mPlan +
            ", mTraveller=" + mTraveller +
            ", mContactName=" + mContactName +
            ", mContactPhone=" + mContactPhone +
            ", mState=" + mState +
            ", mPayImageUrl=" + mPayImageUrl +
        "}";
    }
}
