package com.qianxun.utils;

public class StringUtils {
    public static boolean isNumber(String str) {
        if(isEmpty(str))
            return false;
        for (int i = str.length(); --i >= 0; ) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isEmpty(String str) {
        if(str==null || "".equals(str)){
            return true;
        }
        return false;
    }
}
