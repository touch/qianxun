package com.qianxun.mapper;

import com.qianxun.entity.Tag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author haitang
 * @since 2021-12-05
 */
public interface TagMapper extends BaseMapper<Tag> {

}
